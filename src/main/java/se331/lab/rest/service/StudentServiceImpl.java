package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    StudentDao studentDao;
    @Override
    public List<Student> getStudents() {
        return this.studentDao.getStudents();
    }

    @Override
    public Student getStudent(Long id) {
        return this.studentDao.getStudent(id);
    }

    @Override
    public Student saveStudent(Student student) {
        return this.studentDao.saveStudent(student);
    }
}
